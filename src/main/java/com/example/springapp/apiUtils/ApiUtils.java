package com.example.springapp.apiUtils;

import com.example.springapp.dto.ApiResponse;

public class ApiUtils {

    public static ApiResponse getApiResponse(Object object) {

        String name  = object.getClass().getSimpleName();
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.addData(name, object);
        return apiResponse;

    }
}
