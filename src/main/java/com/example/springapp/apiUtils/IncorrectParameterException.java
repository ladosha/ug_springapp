package com.example.springapp.apiUtils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashSet;
import java.util.Set;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IncorrectParameterException extends RuntimeException {
    private Set<String> incorrectParameters = new HashSet();

    public IncorrectParameterException() {
        super("INCORRECT_PARAMETER_EXCEPTION");
    }

    public IncorrectParameterException addIncorrectParameter(String parameter) {
        this.incorrectParameters.add(parameter);
        return this;
    }

    public IncorrectParameterException addIncorrectParameters(Set<String> parameters) {
        this.incorrectParameters.addAll(parameters);
        return this;
    }
}
