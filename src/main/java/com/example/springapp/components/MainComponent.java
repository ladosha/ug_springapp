package com.example.springapp.components;

import com.example.springapp.services.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class MainComponent {

    public static final Logger logger = LoggerFactory.getLogger(MainComponent.class);

    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");

    // service wasn't really needed, but if we write it we can import
    // only one thing in the component
    private final StatisticsService statisticsService;

    @Autowired
    public MainComponent(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @Scheduled(fixedDelay = 5000)
    public void reportCurrentTime() {
        logger.info("The time is now {}", dateFormatter.format(new Date()));
    }

//    @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.HOURS)
//    public void saveEmployeeStats() {
//        Long empCount = statisticsService.addStatisticsEntry();
//
//        logger.info("We have {} employees", empCount);
//    }

    @Scheduled(cron = "0 15 10 3 *")
    public void saveEmployeeStats() {
        Long empCount = statisticsService.addStatisticsEntry();

        logger.info("We have {} employees", empCount);
    }
}
