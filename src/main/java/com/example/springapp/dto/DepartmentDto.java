package com.example.springapp.dto;

import lombok.Data;

@Data
public class DepartmentDto {
    private Long departmentId;
    private String departmentName;
}
