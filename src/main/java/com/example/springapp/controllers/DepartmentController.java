package com.example.springapp.controllers;

import com.example.springapp.dto.ApiResponse;
import com.example.springapp.dto.DepartmentDto;
import com.example.springapp.jpa.entities.Department;
import com.example.springapp.services.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {
    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {

        this.departmentService = departmentService;
    }

    @PostMapping("/department/add")
    public ApiResponse addDepartment(@RequestBody DepartmentDto departmentDto) {
        return departmentService.addDepartment(departmentDto);
    }

    @PostMapping("/department/search")
    public List<Department> searchDepartments(@RequestBody Department department) {
        return departmentService.searchDepartment(department);
    }

    @GetMapping("/department/get/{id}")
    public ApiResponse getDepartmentById(@PathVariable Long id) {
        return departmentService.getDepartmentById(id);
    }

    @GetMapping("/department/get/all")
    public ApiResponse getDepartmentById() {
        return departmentService.getAllDepartments();
    }

    @PutMapping("/department/update/{departmentId}")
    public ApiResponse updateDepartment(@RequestBody DepartmentDto departmentDto, @PathVariable Long departmentId) {
        return departmentService.updateDepartments(departmentDto, departmentId);
    }

    @DeleteMapping("/department/delete/{departmentId}")
    public ApiResponse deleteDEpartment(@PathVariable Long departmentId) {
        return departmentService.deleteDepartment(departmentId);
    }

    @GetMapping("/employee/getbyname/{empName}")
    public ApiResponse getEmployeeByName(@PathVariable String empName) {
        return departmentService.getEmployee(empName);
    }

    @GetMapping("/employee/getsalarymorethan/{lowerBound}")
    public ApiResponse getEmployeesWithBigSalary(@PathVariable Integer lowerBound) {
        return departmentService.getEmployeeWithBigSalary(lowerBound);
    }
}
