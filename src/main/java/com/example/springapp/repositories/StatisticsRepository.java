package com.example.springapp.repositories;

import com.example.springapp.jpa.entities.Employee;
import com.example.springapp.jpa.entities.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface StatisticsRepository extends JpaRepository<Statistics, Long>, JpaSpecificationExecutor<Statistics> {

}
