package com.example.springapp.repositories;

import com.example.springapp.projections.EmployeeClassProjection;
import com.example.springapp.projections.EmployeeProjection;
import com.example.springapp.jpa.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {

    List<EmployeeProjection> getEmployeeByFullName(String name);

    List<EmployeeClassProjection> getEmployeeBySalaryGreaterThan(Integer lowerBound);
}
