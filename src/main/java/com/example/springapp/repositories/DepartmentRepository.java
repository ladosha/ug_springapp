package com.example.springapp.repositories;

import com.example.springapp.jpa.entities.Department;
import com.example.springapp.jpa.entities.RecordState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface DepartmentRepository extends JpaRepository<Department, Long>, JpaSpecificationExecutor<Department> {

    List<Department> findAllByName(String name);

    Optional<Department> findByIdAndRecordState(Long id, RecordState recordState);

}
