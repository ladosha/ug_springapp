package com.example.springapp.projections;

import java.util.Objects;

public class EmployeeClassProjection {
    private String fullName;
    private Integer salary;

    public EmployeeClassProjection(String fullName, Integer salary) {
        this.fullName = fullName;
        this.salary = salary;
    }

    public String getFullName() {
        return fullName;
    }

    public Integer getSalary() {
        return salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeClassProjection that = (EmployeeClassProjection) o;
        return Objects.equals(fullName, that.fullName) && Objects.equals(salary, that.salary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName, salary);
    }
}
