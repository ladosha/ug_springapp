package com.example.springapp.services;

import com.example.springapp.projections.EmployeeClassProjection;
import com.example.springapp.projections.EmployeeProjection;
import com.example.springapp.apiUtils.ApiUtils;
import com.example.springapp.apiUtils.IncorrectParameterException;
import com.example.springapp.apiUtils.NoSuchElementFoundException;
import com.example.springapp.dto.ApiResponse;
import com.example.springapp.dto.DepartmentDto;
import com.example.springapp.jpa.entities.Department;
import com.example.springapp.jpa.specifications.DepartmentSpecifications;
import com.example.springapp.repositories.DepartmentRepository;
import com.example.springapp.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    private final EmployeeRepository employeeRepository;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;

        this.employeeRepository = employeeRepository;
    }

    public Object addDepartment(Department department) {
        if(department == null || department.getName() == null) {
            return new Exception("NO ARGS");
        }

        try{
            return this.departmentRepository.save(department);
        }
        catch(Exception e) {
            return e;
        }
    }

    public List<Department> searchDepartment(Department department) {
        Specification<Department> specification = Specification
                .where(DepartmentSpecifications.idNotNull());

        if(department.getId() != null) {
            specification = specification.and(DepartmentSpecifications.idEquals(department.getId()));
        }

        return this.departmentRepository.findAll(specification);
    }

    public ApiResponse getDepartmentById(Long departmentId) {
        if(departmentId == null || departmentId <= 0) {
            throw new IncorrectParameterException();
        }

        Optional<Department> department = this.departmentRepository.findById(departmentId);

        if(department.isEmpty()) {
            throw new NoSuchElementFoundException().addDescription("department", "by id");
        }

        return new ApiResponse().addData("department", department.get());
    }

    public ApiResponse addDepartment(DepartmentDto departmentDto) {

        if(departmentDto == null || departmentDto.getDepartmentName() == null ||
        departmentDto.getDepartmentName().isEmpty()) {
            throw new IncorrectParameterException().addIncorrectParameter("department");
        }

        Department department = departmentRepository.save(new Department(departmentDto));

        return ApiUtils.getApiResponse(department);
    }

    public ApiResponse getAllDepartments() {
        List<Department> departments = departmentRepository.findAll();

        return new ApiResponse().addData("departmnets", departments);
    }

    public ApiResponse updateDepartments(DepartmentDto departmentDto, Long departmentId) {
        if(departmentDto == null || departmentDto.getDepartmentName() == null || departmentDto.getDepartmentName().isEmpty()) {
            throw new IncorrectParameterException().addIncorrectParameter("departmentName");
        }

        if(departmentId == null || departmentId <= 0) {
            throw new IncorrectParameterException().addIncorrectParameter("id");
        }

        Optional<Department> department = this.departmentRepository.findById(departmentId);

        if(department.isEmpty()) {
            throw new NoSuchElementFoundException();
        }

        department.get().setName(departmentDto.getDepartmentName());

        return ApiUtils.getApiResponse(departmentRepository.save(department.get()));
    }

    public ApiResponse deleteDepartment(Long id) {
        if(id == null || id <=0) {
            throw new IncorrectParameterException().addIncorrectParameter("id");
        }

        Optional<Department> department = this.departmentRepository.findById(id);

        if(department.isEmpty()) {
            throw new NoSuchElementFoundException();
        }

        this.departmentRepository.deleteById(id);

        return new ApiResponse().addData("success", true);
    }


    public ApiResponse getEmployee(String name) {
        List<EmployeeProjection> employees = this.employeeRepository.getEmployeeByFullName(name);

        return new ApiResponse().addData("employees", employees);
    }

    public ApiResponse getEmployeeWithBigSalary(Integer lowerBound) {
        List<EmployeeClassProjection> employees = this.employeeRepository.getEmployeeBySalaryGreaterThan(lowerBound);

        return new ApiResponse().addData("employees", employees);
    }
}
