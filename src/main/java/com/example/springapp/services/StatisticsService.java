package com.example.springapp.services;
import com.example.springapp.dto.ApiResponse;
import com.example.springapp.jpa.entities.Department;
import com.example.springapp.jpa.entities.Statistics;
import com.example.springapp.repositories.DepartmentRepository;
import com.example.springapp.repositories.EmployeeRepository;
import com.example.springapp.repositories.StatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class StatisticsService {

    private final EmployeeRepository employeeRepository;

    private final StatisticsRepository statisticsRepository;

    @Autowired
    public StatisticsService(EmployeeRepository employeeRepository, StatisticsRepository statisticsRepository) {
        this.employeeRepository = employeeRepository;

        this.statisticsRepository = statisticsRepository;
    }

    public Long addStatisticsEntry() {

        Long empCount = this.employeeRepository.count();

        Statistics statisticsEntry = new Statistics();
        statisticsEntry.setInsertionDate(new Date());
        statisticsEntry.setEmpCount(empCount);

        statisticsRepository.save(statisticsEntry);

        return empCount;
    }

}
