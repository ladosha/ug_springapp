package com.example.springapp.jpa.specifications;

import com.example.springapp.jpa.entities.Department;
import org.springframework.data.jpa.domain.Specification;

public class DepartmentSpecifications {

    private DepartmentSpecifications() {

    }

    public static Specification<Department> idEquals(Long id) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id));
    }

    public static Specification<Department> idNotNull() {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("id")));
    }
}
