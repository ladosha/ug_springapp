package com.example.springapp.jpa.specifications;

import com.example.springapp.jpa.entities.Employee;
import com.example.springapp.jpa.entities.RecordState;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public final class EmployeeSpecifications {


    private EmployeeSpecifications() {

    }

    public static Specification<Employee> idEquals(Long id) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
    }

    public static Specification<Employee> recordStateIn(List<RecordState> recordStates) {
        return (root, query, criteriaBuilder) -> root.get("recordState").in(recordStates);
    }

    public static Specification<Employee> recordStateIsActive(List<RecordState> recordStates) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("recordState"), RecordState.ACTIVE);
    }

    public static Specification<Employee> fullNameLike(String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("fullName"), "%" + name.replaceAll("\\s", "%") + "%");
    }
}
