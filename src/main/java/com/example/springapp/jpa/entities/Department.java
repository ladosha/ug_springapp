package com.example.springapp.jpa.entities;

import com.example.springapp.dto.DepartmentDto;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "DEPARTMENTS")
public class Department extends BaseEntity<Long> {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "departmentIdSeq", sequenceName = "DEPARTMENT_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departmentIdSeq")
    private Long id;



    @Column(name= "NAME", nullable = false)
    private String name;


    public Department(DepartmentDto departmentDto) {
        this.name = departmentDto.getDepartmentName();
    }
}
