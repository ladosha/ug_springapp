package com.example.springapp.jpa.entities;

public enum RecordState {
    DRAFT, ACTIVE, INACTIVE, DELETED
}
