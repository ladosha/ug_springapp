package com.example.springapp.jpa.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "STATISTICS")
public class Statistics {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "employeeIdSeq", sequenceName = "EMPLOYEE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departmentIdSeq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="TIME", updatable = false)
    private Date insertionDate;

    @Column(name = "EMP_COUNT")
    private Long empCount;


}
