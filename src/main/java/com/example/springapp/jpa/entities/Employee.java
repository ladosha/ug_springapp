package com.example.springapp.jpa.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "EMPLOYEES")
public class Employee extends BaseEntity<Long> {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "employeeIdSeq", sequenceName = "EMPLOYEE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departmentIdSeq")
    private Long id;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "SALARY")
    private Integer salary;

    @ManyToOne
    private Department department;

}
